<?php

use App\Actions\Quotes\QuoteGetRandomeAction;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return Inertia::render('Home', [
//        'quote' => \App\Http\Resources\QuoteResource::make(QuoteGetRandomeAction::run())
//    ]);
//});

Route::get('/checkin/{user}', function (\Illuminate\Http\Request $request, \App\Models\User $user) {
    if (!$request->hasValidSignature()) {
        abort(401);
    }

    \Illuminate\Support\Facades\Auth::login($user);

    return redirect(\Filament\Facades\Filament::getUrl());
})->name('checkin');

//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//    return Inertia::render('Dashboard');
//})->name('dashboard');
