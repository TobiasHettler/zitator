<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandList;
use App\Discord\Commands\DiscordCommandQuiz;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandRank;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Discord\Discord;
use Discord\Parts\WebSockets\MessageReaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Tests\Feature\Discord\Fixtures\DiscordCommandTestFixture;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandQuizTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;


    /** @test */
    public function quiz_command_returns_a_quiz()
    {
        Quote::factory()->count(15)->create();
        $quote = Quote::factory()->create(['author' => 'Tobi']);

        $command = (new DiscordCommandQuiz($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "quiz",
        ]), new DiscordGatewayClient()));
        $response = $command->handle();
        $this->assertNotNull($command->meta['quote']);
        $this->assertNotNull($command->meta['quizUuid']);
        $this->assertCount('4', $command->meta['answerOptions']);
        $this->assertNotNull($response->embeds[0]);
        $this->assertNotNull($response->embeds[0]->title);
        $this->assertNotNull($response->embeds[0]->description);
    }

}
