<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandSave;
use App\Discord\Commands\DiscordCommandUpdate;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Models\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandUpdateTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;

    /** @test */
    public function discord_command_gets_triggerd()
    {
        $quote = Quote::factory()->create();
        $this->setUpGateway($this->createCommandRequestPayload(
            'update',
            [$this->createOption('uuid', $quote->uuid)])
        );

        Http::assertSent(fn(Request $request
        ) => $request->data()['data']['embeds'][0]['title'] === '"'.$quote->text.'"');
        $this->assertDatabaseHas('quotes', ['text' => $quote->text]);
    }

    /** @test */
    public function update_command_saves_and_returns_quote()
    {

        $quote = Quote::factory()->create([
            'author' => 'Old',
            'text' => 'Old Quote',
        ]);

        $response = (new DiscordCommandUpdate(
         $this->createInteraction([
                "id" => "817772729754714161",
                "name" => "update",
                "options" => [
                    json_decode(json_encode([
                        "name" => "uuid",
                        "type" => 3,
                        "value" => $quote->uuid,
                    ])),
                    json_decode(json_encode([
                        "name" => "author",
                        "type" => 3,
                        "value" => "Li",
                    ])),

                    json_decode(json_encode([
                        "name" => "quote",
                        "type" => 3,
                        "value" => "Da seh ich uns!",
                    ])),
                ],
            ]), new DiscordGatewayClient()
        ))->handle();

        $quote = $quote->fresh();

        $this->assertEquals('"Da seh ich uns!"', $response->embeds[0]->title);
        $this->assertDatabaseHas('quotes', ['text' => 'Da seh ich uns!']);

        $this->assertEquals('Li', $quote->author);
        $this->assertEquals('Da seh ich uns!', $quote->text);
    }


    /** @test */
    public function update_command_saves_nothing_if_nothing_is_there()
    {

        $quote = Quote::factory()->create([
            'author' => 'Old',
            'text' => 'Old Quote',
        ]);

        $response = (new DiscordCommandUpdate(
            $this->createInteraction([
                    "id" => "817772729754714161",
                    "name" => "update",
                    "options" => [
                        json_decode(json_encode([
                            "name" => "uuid",
                            "type" => 3,
                            "value" => $quote->uuid,
                        ])),
                    ],
                ]),
            new DiscordGatewayClient()
        ))->handle();

        $quote = $quote->fresh();

        $this->assertEquals('"Old Quote"', $response->embeds[0]->title);
        $this->assertDatabaseHas('quotes', ['text' => 'Old Quote']);

        $this->assertEquals('Old', $quote->author);
        $this->assertEquals('Old Quote', $quote->text);
    }
}
