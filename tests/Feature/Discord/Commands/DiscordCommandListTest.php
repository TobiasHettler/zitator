<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandList;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandRank;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Tests\Feature\Discord\Fixtures\DiscordCommandTestFixture;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandListTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;


    /** @test */
    public function discord_command_gets_triggerd()
    {
        $quote = Quote::factory()->create();
        $this->setUpGateway($this->createCommandRequestPayload('list'));

        Http::assertSent(fn(Request $request) => $request->data()['data']['content'] !== '');
    }

    /** @test */
    public function list_command_returns_the_quotes()
    {
        Quote::factory()->count(15)->create(['author' => 'monet']);
        Quote::factory()->count(10)->create(['author' => 'Flausch']);
        Quote::factory()->count(5)->create(['author' => 'Tobi']);

        $response = (new DiscordCommandList($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "list",
        ]), new DiscordGatewayClient()))->handle();

        $this->assertNotNull($response->content);
    }

    /** @test */
    public function list_command_returns_the_quotes_in_random_order()
    {
        Quote::factory()->count(15)->create(['author' => 'monet']);
        Quote::factory()->count(10)->create(['author' => 'Flausch']);
        Quote::factory()->count(5)->create(['author' => 'Tobi']);

        $response1 = (new DiscordCommandList($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "list",
        ]), new DiscordGatewayClient()))->handle();

        $response2 = (new DiscordCommandList($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "list",
        ]), new DiscordGatewayClient()))->handle();


        $this->assertNotEquals($response1->content, $response2->content);
    }

    /** @test */
    public function list_also_returns_the_ids()
    {
        Quote::factory()->count(15)->create(['author' => 'monet']);
        Quote::factory()->count(10)->create(['author' => 'Flausch']);
        Quote::factory()->count(5)->create(['author' => 'Tobi']);

        $response = (new DiscordCommandList($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "list",
            'options' => [
                json_decode(json_encode([
                    "name" => "withid",
                    "type" => 5,
                    "value" => true,
                ]))
            ]
        ]), new DiscordGatewayClient()))
            ->handle();
        $this->assertTrue(Str::contains($response->content, 'ID'));
    }

}
