<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandRegister;
use App\Discord\Commands\DiscordCommandSave;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandRegisterTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;

    /** @test */
    public function discord_command_gets_triggerd()
    {
        $this->setUpGateway($this->createCommandRequestPayload(
            'register',
            [$this->createOption('email', 'test@bla.de'), $this->createOption('password', 'password'),])
        );

        Http::assertSent(fn(Request $request) => $request->data()['data']['embeds'][0]['title'] === 'Success! Click here and visit the dashboard.');
        $this->assertDatabaseHas('users', ['email' => 'test@bla.de']);
    }

    /** @test */
    public function register_command_registers_new_user()
    {
        $quote = (new DiscordCommandRegister($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "register",
            "options" => [
                json_decode(json_encode([
                    "name" => "email",
                    "type" => 3,
                    "value" => "Li@bla.de",
                ])),
                json_decode(json_encode([
                    "name" => "password",
                    "type" => 3,
                    "value" => "password",
                ]))
            ],
        ], fromChannel:  false), new DiscordGatewayClient()))
            ->handle();

        $this->assertEquals('Success! Click here and visit the dashboard.', $quote->embeds[0]->title);
        $this->assertDatabaseHas('users', ['email' => 'Li@bla.de']);
    }

    /** @test */
    public function register_command_must_be_called_in_pm()
    {
        $quote = (new DiscordCommandRegister($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "register",
            "options" => [
                json_decode(json_encode([
                    "name" => "email",
                    "type" => 3,
                    "value" => "Li@bla.de",
                ])),
                json_decode(json_encode([
                    "name" => "password",
                    "type" => 3,
                    "value" => "password",
                ]))
            ],
        ], fromChannel:  true), new DiscordGatewayClient()))
            ->handle();

        $this->assertEquals('This command needs to be run in Zitators private messages.', $quote->embeds[0]->title);
        $this->assertDatabaseMissing('users', ['email' => 'Li@bla.de']);
    }

    /** @test */
    public function register_command_must_has_valid_email()
    {
        $quote = (new DiscordCommandRegister($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "register",
            "options" => [
                json_decode(json_encode([
                    "name" => "email",
                    "type" => 3,
                    "value" => "Libla.de",
                ])),
                json_decode(json_encode([
                    "name" => "password",
                    "type" => 3,
                    "value" => "password",
                ]))
            ],
        ], fromChannel:  false), new DiscordGatewayClient()))
            ->handle();

        $this->assertEquals('Email validation Error!', $quote->embeds[0]->title);
        $this->assertDatabaseMissing('users', ['email' => 'Li@bla.de']);
    }
}
