<?php


namespace Tests\Feature\Discord\Commands;


use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\Commands\DiscordCommand;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\Exceptions\CommandException;
use App\Discord\Exceptions\CommandValidationException;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class DiscordCommandTest extends TestCase
{

    /** @test */
    public function discord_command_correctly_initalizes_options()
    {
        $discordData = $this->getDiscordInteractionCommandTransfer();

        $testCommand = new class ($discordData, new DiscordGatewayClient()) extends DiscordCommand {
            #[CommandOption(help: 'Test')]
            public string $option_1;

            #[CommandOption(help: 'Test')]
            public bool $option_2;

            public string $no_option = 'Nothing Happens here';

            public function handle(): CommandResponse
            {
                // TODO: Implement handle() method.
            }
        };


        $this->assertEquals('Option 1', $testCommand->option_1);
        $this->assertTrue($testCommand->option_2);

        $this->assertEquals('Nothing Happens here', $testCommand->no_option);
    }

    /** @test */
    public function discord_command_options_can_have_differnet_names_than_properties()
    {
        $discordData = $this->getDiscordInteractionCommandTransfer();

        $testCommand = new class ($discordData, new DiscordGatewayClient()) extends DiscordCommand {
            #[CommandOption(
                name: 'option_1',
                help: 'Test'
            )]
            public string $diffrentFromOption;

            public function handle(): CommandResponse
            {
                // TODO: Implement handle() method.
            }
        };


        $this->assertEquals('Option 1', $testCommand->diffrentFromOption);
    }

    /** @test */
    public function discord_command_options_can_throws_exception_if_required_option_dont_exist()
    {
        $discordData = $this->getDiscordInteractionCommandTransfer();

        try {
            new class ($discordData, new DiscordGatewayClient()) extends DiscordCommand {
                #[CommandOption(
                    required: true,
                    help: 'Test'
                )]
                public string $dontExist;

                public function handle(): CommandResponse
                {
                    // TODO: Implement handle() method.
                }
            };

            $this->fail('Option required Exception was not raised.');
        } catch (CommandException $exception) {
            $this->assertInstanceOf(CommandException::class, $exception);
            $this->assertEquals("Option 'dontExist' is required!", $exception->getMessage());
        }

    }

    /** @test */
    public function discord_command_options_be_validated()
    {
        $discordData = $this->getDiscordInteractionCommandTransfer();

        try {
            new class ($discordData, new DiscordGatewayClient()) extends DiscordCommand {
                #[CommandOption(
                    validate: ['max:3'],
                    help: 'Test'
                )]
                public string $option_1;

                public function handle(): CommandResponse
                {
                    // TODO: Implement handle() method.
                }
            };

            $this->fail('Option validation Exception was not raised.');
        } catch (CommandValidationException $exception) {
            $this->assertInstanceOf(CommandValidationException::class, $exception);
            $this->assertEquals("The option 1 may not be greater than 3 characters.",
                $exception->errors()['option_1'][0]);
        } catch (\Exception $exception) {
            $this->fail('Option validation Exception was not raised.');
        }

    }

    /** @test */
    public function discord_command_options_be_validated_with_custom_messages()
    {
        $discordData = $this->getDiscordInteractionCommandTransfer();

        try {
            new class ($discordData, new DiscordGatewayClient()) extends DiscordCommand {
                #[CommandOption(
                    validate: ['max:3'],
                    validateMessages: ['max' => 'This is custom :attribute'],
                    help: 'Test'
                )]
                public string $option_1;

                public function handle(): CommandResponse
                {
                    // TODO: Implement handle() method.
                }
            };

            $this->fail('Option validation Exception was not raised.');
        } catch (CommandValidationException $exception) {
            $this->assertInstanceOf(CommandValidationException::class, $exception);
            $this->assertEquals("This is custom option 1", $exception->errors()['option_1'][0]);
        } catch (\Exception $exception) {
            $this->fail('Option validation Exception was not raised.');
        }

    }

    protected function getDiscordInteractionCommandTransfer(): \App\Discord\DTOs\DiscordInteractionTransfer
    {
        return $this->createInteraction([
            "id" => "817772729754714161",
            "name" => "quote",
            "options" => [
                json_decode(json_encode([
                    "name" => "option_1",
                    "type" => 3,
                    "value" => "Option 1",
                ])),
                json_decode(json_encode([
                    "name" => "option_2",
                    "type" => 3,
                    "value" => "1",
                ])),
            ],
        ]);
    }

}
