<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandSave;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandSaveTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;

    /** @test */
    public function discord_command_gets_triggerd()
    {
        $this->setUpGateway($this->createCommandRequestPayload(
            'save',
            [$this->createOption('author', 'test'), $this->createOption('quote', 'test'),])
        );

        Http::assertSent(fn(Request $request) => $request->data()['data']['embeds'][0]['title'] === '"test"');
        $this->assertDatabaseHas('quotes', ['text' => 'test']);

    }

    /** @test */
    public function save_command_saves_and_returns_quote()
    {
        $quote = (new DiscordCommandSave($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "quote",
            "options" => [
                json_decode(json_encode([
                    "name" => "author",
                    "type" => 3,
                    "value" => "Li",
                ])),
                json_decode(json_encode([
                    "name" => "quote",
                    "type" => 3,
                    "value" => "Da seh ich uns!",
                ]))
            ],
        ]), new DiscordGatewayClient()))->handle();

        $this->assertEquals('"Da seh ich uns!"', $quote->embeds[0]->title);
        $this->assertDatabaseHas('quotes', ['text' => 'Da seh ich uns!']);

    }
}
