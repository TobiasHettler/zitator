<?php


namespace Tests\Feature\Discord\Commands;

use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Discord\Discord;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandQuoteTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;

    /** @test */
    public function discord_command_gets_triggerd()
    {
        $quote = Quote::factory()->create();
        $this->setUpGateway($this->createCommandRequestPayload('quote'));

        Http::assertSent(fn(Request $request) => $request->data()['data']['embeds'][0]['title'] === '"'.$quote->text.'"');
    }

    /** @test */
    public function quote_command_returns_a_randome_quote()
    {
        Quote::factory()->count(15)->create();

        $diffrentQuiteCont = collect(range(0, 10))
            ->map(fn() => (new DiscordCommandQuote($this->createInteraction([
                "id" => "817772729754714161",
                "name" => "quote"
            ]), new DiscordGatewayClient()))
                ->handle()
                ->embeds[0]
                ->title)
            ->unique()
            ->count();

        $this->assertGreaterThan(3, $diffrentQuiteCont);
    }

    /** @test */
    public function quote_command_can_accept_an_author()
    {
        Quote::factory()->count(10)->create(['author' => 'Tobi']);
        Quote::factory()->create(['author' => 'Monet', 'text' => 'Eier.']);

        $quote = (new DiscordCommandQuote($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "quote",
            "options" => [
                json_decode(json_encode([
                    "name" => "user",
                    "type" => 3,
                    "value" => "Monet",
                ]))
            ],
        ]), new DiscordGatewayClient()))->handle()
            ->embeds[0];

        $this->assertEquals('"Eier."', $quote->title);
        $this->assertEquals('Monet - '.now()->format('F Y'), $quote->footer['text']);
    }
}
