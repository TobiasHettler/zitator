<?php


namespace Tests\Feature\Discord\Commands;

use App\Discord\Attributes\Command;
use App\Discord\Commands\DiscordCommandRank;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Models\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Tests\Support\UseDiscordCommandRequest;
use Tests\TestCase;

class DiscordCommandRankTest extends TestCase
{
    use RefreshDatabase, UseDiscordCommandRequest;

    /** @test */
    public function discord_command_gets_triggerd()
    {
        $quote = Quote::factory()->create();
        $this->setUpGateway($this->createCommandRequestPayload('rank'));

        Http::assertSent(fn(Request $request) => count($request->data()['data']['embeds']) === 2);
    }

    /** @test */
    public function rank_command_returns_the_ranks()
    {
        Quote::factory()->count(15)->create(['author' => 'monet']);
        Quote::factory()->count(10)->create(['author' => 'Flausch']);
        Quote::factory()->count(5)->create(['author' => 'Tobi']);


        $response = (new DiscordCommandRank($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "rank",
        ]), new DiscordGatewayClient()))->handle();
        $podium = $response->embeds[0];
        $table = $response->embeds[1];

        $this->assertEquals(':trophy: monet is the Zitator :trophy:', $podium->title);
        $this->assertEquals('__________________', $podium->description);

        $this->assertEquals(':first_place: monet', $podium->fields[0]['name']);
        $this->assertEquals('15 Quotes', $podium->fields[0]['value']);
        $this->assertEquals(true, $podium->fields[0]['inline']);

        $this->assertEquals(':second_place: Flausch', $podium->fields[1]['name']);
        $this->assertEquals('10 Quotes', $podium->fields[1]['value']);
        $this->assertEquals(true, $podium->fields[1]['inline']);

        $this->assertEquals(':third_place: Tobi', $podium->fields[2]['name']);
        $this->assertEquals('5 Quotes', $podium->fields[2]['value']);
        $this->assertEquals(true, $podium->fields[2]['inline']);

        $this->assertEquals(
            '```┌──────┬─────────┬────────┐
│ RANK │ AUTHOR  │ QUOTES │
├──────┼─────────┼────────┤
│ 1    │ monet   │ 15     │
│ 2    │ Flausch │ 10     │
│ 3    │ Tobi    │ 5      │
└──────┴─────────┴────────┘
```',
            $table->description
        );
    }

    /** @test */
    public function rank_command_dont_breaks_if_no_quotes_exist()
    {
        $response = (new DiscordCommandRank($this->createInteraction([
            "id" => "817772729754714161",
            "name" => "rank",
        ]), new DiscordGatewayClient()))->handle();

        $this->assertEquals('There are no Quotes, yet.', $response->content);
    }

}
