<?php


namespace Tests\Feature\Discord\Fixtures;


use App\Discord\Attributes\Command;
use App\Discord\CommandResponse;
use App\Discord\Commands\DiscordCommand;

#[Command(name: 'test-command')]
class DiscordCommandTestFixture extends DiscordCommand
{
    public function handle(): CommandResponse
    {
        return $this->respond($this->commandRequest->name);
    }
}
