<?php

namespace Tests;

use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\DTOs\DiscordInteractionTransfer;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createInteraction(array $data, bool $fromChannel = true): DiscordInteractionTransfer
    {
        $user = [
            "avatar" => "020a1b165af9ea61bdaed99807d69d24",
            "discriminator" => "9885",
            "id" => "278252969432973312",
            "public_flags" => 0,
            "username" => "FreshTae (Tobi)"
        ];

        $data = [
            'id' => '21',
            'type' => 1,
            'token' => 'asdasd',
            'version' => 2,
            'data' => new DiscordInteractionCommandTransfer($data)
        ];

        if ($fromChannel) {
            $data['member'] = (object)['user' => $user];
        } else {
            $data['user'] = (object) $user;
        }

        return new DiscordInteractionTransfer($data);
    }
}
