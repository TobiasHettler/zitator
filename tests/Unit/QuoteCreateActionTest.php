<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteCreateActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function quote_create_action_saves_Quote_to_db()
    {
        /** @var Quote $qoute */
       $qoute = QuoteCreateAction::run(new QuoteCreateTransfer([
            'text' => 'Eibenholz ist quasi erektile Dysfunktion.',
            'author' => 'Hannes'
        ]));

       $this->assertEquals('Eibenholz ist quasi erektile Dysfunktion.', $qoute->text);
       $this->assertEquals('Hannes', $qoute->author);

       $this->assertDatabaseHas('quotes', ['text' => 'Eibenholz ist quasi erektile Dysfunktion.']);
    }
}
