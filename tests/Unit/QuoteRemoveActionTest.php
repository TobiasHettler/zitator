<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteRemoveAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteRemoveTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteRemoveActionTest  extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function quote_remove_action_removes_quote_from_db()
    {

       $quote = Quote::factory()->create();

        $this->assertDatabaseCount('quotes', 1);

        /** @var Quote $qoute */
       QuoteRemoveAction::run(new QuoteRemoveTransfer([
            'uuid' => $quote->uuid,
        ]));

        $this->assertDatabaseCount('quotes', 0);
    }
}
