<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteRemoveAction;
use App\Actions\Quotes\QuoteUpdateAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteRemoveTransfer;
use App\Transfers\QuoteUpdateTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteUpdateActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function quote_update_action_updates_quote_in_db()
    {
        $quote = Quote::factory()->create();

        $this->assertDatabaseHas('quotes', ['text' => $quote->text, 'author' => $quote->author]);

        /** @var Quote $qoute */
        QuoteUpdateAction::run(new QuoteUpdateTransfer([
            'uuid' => $quote->uuid,
            'author' => 'New Author',
            'text' => 'New Text',
        ]));

        $this->assertDatabaseMissing('quotes', ['text' => $quote->text, 'author' => $quote->author]);
        $this->assertDatabaseHas('quotes', ['text' => 'New Text', 'author' => 'New Author']);
    }
}
