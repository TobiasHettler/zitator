<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Actions\Quotes\QuoteListAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteListTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteListActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function get_a_list_of_all_quotes()
    {
        Quote::factory()->count(5)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer());

        $this->assertCount(5, $quotes);
    }

    /** @test */
    public function get_a_list_of_all_quotes_from_a_author()
    {
        Quote::factory()->count(2)->create(['author' => 'Tobias']);
        Quote::factory()->count(5)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'author' => 'obias'
        ]));

        $this->assertCount(2, $quotes);
    }

    /** @test */
    public function get_a_list_of_all_quotes_from_a_author_in_strict_mode()
    {
        Quote::factory()->count(2)->create(['author' => 'Tobias']);
        Quote::factory()->count(5)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'author' => 'obias',
            'strict' => true
        ]));

        $this->assertCount(0, $quotes);

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'author' => 'Tobias',
            'strict' => true
        ]));

        $this->assertCount(2, $quotes);

    }

    /** @test */
    public function get_a_list_of_all_quotes_from_a_text()
    {
        Quote::factory()->count(2)->create(['text' => 'Tobias isst Obst.']);
        Quote::factory()->count(5)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'text' => 'isst Ob'
        ]));

        $this->assertCount(2, $quotes);
    }

    /** @test */
    public function get_a_list_of_all_quotes_from_a_text_in_strict_mode()
    {
        Quote::factory()->count(2)->create(['author' => 'Tobias isst Obst.']);
        Quote::factory()->count(5)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'text' => 'isst Ob',
            'strict' => true
        ]));

        $this->assertCount(0, $quotes);

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'author' => 'Tobias isst Obst.',
            'strict' => true
        ]));

        $this->assertCount(2, $quotes);
    }

    /** @test */
    public function get_a_list_of_ordert_quotes()
    {
        Quote::factory()->create(['text' => 'BBB']);
        Quote::factory()->create(['text' => 'CCC']);
        Quote::factory()->create(['text' => 'AAA']);

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'orderBy' => 'text'
        ]));

        $this->assertEquals(['AAA', 'BBB', 'CCC'], $quotes->pluck('text')->all());

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'orderBy' => 'text',
            'orderDirection' => 'desc'
        ]));

        $this->assertEquals(['CCC', 'BBB', 'AAA',], $quotes->pluck('text')->all());
    }

    /** @test */
    public function get_a_list_of_limited_quotes()
    {
        Quote::factory()->count(30)->create();

        $quotes = QuoteListAction::run(new QuoteListTransfer());
        $this->assertCount(25, $quotes);

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'limit' => 28,
        ]));
        $this->assertCount(28, $quotes);

        $quotes = QuoteListAction::run(new QuoteListTransfer([
            'limit' => 35,
        ]));
        $this->assertCount(30, $quotes);
    }
}
