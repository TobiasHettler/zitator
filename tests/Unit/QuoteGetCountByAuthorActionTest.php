<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteGetCountByAuthorActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function count_quotes_by_author()
    {
        Quote::factory()->count(15)->create(['author' => 'Monet']);
        Quote::factory()->count(10)->create(['author' => 'Tobi']);

        $result = QuoteGetCountByAuthorAction::run()->toArray();

        $this->assertEquals([
            [
                "count" => 15,
                "author" => "Monet",
            ],
            [
                "count" => 10,
                "author" => "Tobi",
            ]
        ], $result);
    }
}
