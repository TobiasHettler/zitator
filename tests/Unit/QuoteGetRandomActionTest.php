<?php

namespace Tests\Unit;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuoteGetRandomActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function get_random_quote_from_db()
    {
        Quote::factory()->count(15)->create();

        $diffrentQuiteCont = collect(range(0, 10))->map(fn() => QuoteGetRandomeAction::run()->uuid)->unique()->count();

        $this->assertGreaterThan(3, $diffrentQuiteCont);
    }

    /** @test */
    public function get_random_quote_from_author()
    {
        Quote::factory()->count(15)->create(['author' => 'Monet']);
        Quote::factory()->count(15)->create();

        $diffrentQuiteCont = collect(range(0, 10))->map(fn() => QuoteGetRandomeAction::run('Monet')->uuid)->unique()->count();

        $this->assertGreaterThan(3, $diffrentQuiteCont);
    }
}
