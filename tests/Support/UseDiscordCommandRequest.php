<?php


namespace Tests\Support;


use App\Discord\DiscordGatewayClient;

trait UseDiscordCommandRequest
{
    public function setUpGateway(\stdClass $payload)
    {
        \Illuminate\Support\Facades\Http::fake();
        $message = new \stdClass();
        $message->t = DiscordGatewayClient::INTERACTION_CREATE;
        $message->d = $payload;

        $client = (new DiscordGatewayClient());
        $client->discord->emit('ready', [$client->discord]);
        $client->discord->emit('raw', [$message, $client->discord]);
    }

    public function createOption(string $name, mixed $value, int $type = 3): \stdClass
    {
        $option = new \stdClass();
        $option->name = $name;
        $option->type = $type;
        $option->value = $value;
        return $option;
    }

    public function createCommandRequestPayload(string $commandName = 'test-command', array $options = []): \stdClass
    {
        return json_decode(json_encode([
            "channel_id" => "803708325274451970",
            "data" => [
                "id" => "817773402823721040",
                "name" => $commandName,
                "options" => $options,
            ],
            "guild_id" => "803708324754882581",
            "id" => "817827028119846932",
            "user" => [
                "avatar" => "020a1b165af9ea61bdaed99807d69d24",
                "discriminator" => "9885",
                "id" => "278252969432973312",
                "public_flags" => 0,
                "username" => "FreshTae (Tobi)"
            ],
            "member" => [
                "deaf" => false,
                "is_pending" => false,
                "joined_at" => "2021-01-26T19=>29=>56.880000+00=>00",
                "mute" => false,
                "nick" => null,
                "pending" => false,
                "permissions" => "8589934591",
                "premium_since" => null,
                "roles" => [],
                "user" => [
                    "avatar" => "020a1b165af9ea61bdaed99807d69d24",
                    "discriminator" => "9885",
                    "id" => "278252969432973312",
                    "public_flags" => 0,
                    "username" => "FreshTae (Tobi)"
                ]
            ],
            "token" => "aW50ZXJhY3Rpb246ODE3ODI3MDI4MTE5ODQ2OTMyOjlhS2RLQWQ0b1h0RDVsbkdnVFlLbmdPeHU3QkxsblIyekFZUlFFTFk2aXpQZ0FQN0o0N0x6R0I0MUQzR1BSY0tUNVA3UTVVdHZCSEl2TExsZVVOdTBBTDhKTEFORVF5RmV2MVdyVlhCcDE1M3RXbjU1c0lzczRURjRXbXBCREhN",
            "type" => 2,
            "version" => 1
        ]));
    }
}
