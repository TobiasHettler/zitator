<?php

namespace Database\Seeders;

use App\Actions\Quotes\QuoteCreateAction;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $quotes = [
            ['text' => "Ich kann dir ne tote Taube ans Fenster werfen, wenn du willst", 'author' => "Flausch"],
            ['text' => "Blind und farbenblind", 'author' => "Li"],
            ['text' => "Hallo untenrum!", 'author' => "Monet"],
            ['text' => "In der Zwischenzeit, Dreier?", 'author' => "Flausch"],
            ['text' => "Nichts", 'author' => "Hannes"],
            ['text' => "Monet, ich fühl dich sehr!", 'author' => "Flausch"],
            ['text' => "Ich liebe es, unschuldige Frauen umzubringen.", 'author' => "Flo"],
            ['text' => "Ich hab die neun Schwänze bestellt", 'author' => "Li"],
            ['text' => "So, wir knallen die jetzt richtig im Busch!", 'author' => "Tobi"],
            ['text' => "Du bist irgendwie sneller geschlidet als ich.", 'author' => "Flausch"],
            ['text' => "Ich war so froh, dass es richtig scheiße war.", 'author' => "Flausch"],
            ['text' => "Mein Fuß war im Ofen.", 'author' => "Alena"],
            ['text' => "Was hast du für ein Gehirn?", 'author' => "Alena"],
            ['text' => "Orkan, wieso willst du mich nicht nackt sehen?", 'author' => "Monet"],
            ['text' => "Wenn du vom Richt ledest", 'author' => "Tobi"],
            ['text' => "Dann braucht man [...], eine Schlange für den Schwanz", 'author' => "Flo"],
            ['text' => "Ich hab Kacke nicht aus dem Kopf gekriegt", 'author' => "Tobi"],
            ['text' => "Eibenholz ist quasi erektile Dysfunktion", 'author' => "Hannes"],
            ['text' => "Aber so n Handjob von Mama...", 'author' => "Tobi"],
            ['text' => "Du bist in @MedBae reingegangen", 'author' => "Adrian"],
            ['text' => "Einfach mal wieder was Faxen", 'author' => "Flausch"],
            ['text' => "Rechts anmachen, links töten", 'author' => "Flausch"],
            ['text' => "Was mich am meisten anturnt, ist der Käse auf deinem Kopf", 'author' => "Monet"],
            ['text' => "Ach, man kann es auch von hinten machen", 'author' => "Flausch"],
            ['text' => "Heute ist ein Tag!", 'author' => "Flausch"],
            ['text' => "Einführen kann man vieles", 'author' => "Rafi"],
            ['text' => "Ich hab noch nie einen Gummi benutzt.", 'author' => "Monet"],
            ['text' => "Monet ich bin vor 5 Sekunden gekommen. Und jetzt sogar nochmal.", 'author' => "Orkan"],
            ['text' => "Ich hab keine Eier mehr.", 'author' => "Flausch"],
        ];

        foreach ($quotes as $quote) {
            QuoteCreateAction::run(new QuoteCreateTransfer($quote));
        }
        // \App\Models\User::factory(10)->create();
    }
}
