<?php


namespace App\Actions\Quotes;


use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteUpdateTransfer;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;

class QuoteImportAction
{
    use AsAction;

    public string $commandSignature = 'quote:import';


    public function handle()
    {
        $quotes =
            [
                ["text" => "Ich schwanke zwischen Kuscheln und Vorhaut", "author" => "Orkan"],
                ["text" => "Ich weiß nicht wie Eulen riechen", "author" => "Orkan "],
                ["text" => "Eigentlich bin ich überhaupt keine Maschine", "author" => "Monet"],
                ["text" => "Backkunstauswürfe", "author" => "Honey Pie"],
                ["text" => "Poney High", "author" => "Li"],
                ["text" => "Minotaurus-Positivity", "author" => "Gurkgot"],
                ["text" => "Oh, da ist eine Leiche, da bleibe ich", "author" => "Li"],
                ["text" => "Vagina in alle Löcher", "author" => "Lesus"],
                ["text" => "Er hat mir in den Mund gespritzt", "author" => "Monet"],
                ["text" => "Füße sind einfach hässliche Hände", "author" => "Orkan"],
                ["text" => "Das ist Umzugskarton", "author" => "Max"],
                ["text" => "Ich bin [...] ready for daddy", "author" => "Chokemaster"],
                ["text" => "Ich würde nie baden", "author" => "Monet"],
                ["text" => "Wir sollten alle zusammen baden", "author" => "Alena"],
                ["text" => "Disney's Radieschen", "author" => "Max & Li"],
                ["text" => "Ich hatte schon alles, von viel älter bis viel jünger", "author" => "Flaaun"],
                ["text" => "Crush my head with those thighs, mommy", "author" => "Orkan"],
                ["text" => "Hey, ich bin eine Freude!", "author" => "Lena"],
                ["text" => "[Nichts ist] besser als Cousinen-Bondage", "author" => "Monet"],
                ["text" => "Monet, wie schwer ist dein Schwanz?", 'author' => "Adrian"],
                [
                    "text" => "Wenn du deine rechte Titte abschneidest, könntest du besser Bogenschießen",
                    "author" => "Monet"
                ],
                ["text" => "Ich will deine Tränen sehen", "author" => "Alena"],
                ["text" => "Ich bin gern im Käfig", "author" => "Li"],
                ["text" => "Jetzt wissen wir, wie das Besteck des jeweils anderen aussieht", "author" => "Flaaun"],
                ["text" => "MedBae ist zurückgeblieben", "author" => "Rafi"],
                ["text" => "Trauma, Geld, Butler, was willst du mehr?!", "author" => "Li"],
                ["text" => "Ich bin auch ein Duftbaum", "author" => "Li"],
                ["text" => "Du hast den Penis gestört!", "author" => "Tobi"],
                ["text" => "Ich hab einen Screenshot von unserem Penis gemacht.", "author" => "Lesus"],
                ["text" => "Ich hab gewartet bis du die Lights gefickt hast", "author" => "Tobi"],
                ["text" => "Ich kaue gerne", "author" => "Max "],
                ["text" => "Ich bin immer zu faul das Ding in den Mund zu halten", "author" => "Monet"],
                ["text" => "Kennst du Lena? Ich bin ihre Cousine.", "author" => "Monet"],
                ["text" => "In einer Runde kann ich wieder mit dir schlafen, pass bloß auf!", "author" => "Tobi"],
                ["text" => "Ich komme, AIDS, ich komme!", "author" => "Tobi"],
                [
                    "text" => "Du gibst Eier mit Zimt in eine Schüssel, verrührst es und ziehst dann dein Ei durch.",
                    "author" => "Flausch"
                ],
                ["text" => "Tentacle... Rave", "author" => "Flo"],
                ["text" => "Es fühlt sich so an, als ob mein Hals schimmelt.", "author" => "Flausch"],
                ["text" => "Ich seh nichts Tobi. Mach dein Penis weg", "author" => "Alena"],
                ["text" => "Rafi und ich sind die Cam-Bros", "author" => "Monet"],
                ["text" => "Ich bin literally mit ihm [...]gekommen", "author" => "Adrian"],
                ["text" => "Willst du dich von uns rapen lassen?", 'author' => "Alena"],
                ["text" => "Ich wurde von Monet penetriert", "author" => "Tobi"],
                ["text" => "Monets Zungenspitze war ein bisschen zu spitz für mich", "author" => "Tobi"],
                ["text" => "Monet hat in den Himmel gekackt", "author" => "Lena"],
                ["text" => "Ich bin eine Mutter von 2 Kindern", "author" => "Orkan"],
                ["text" => "Wir können das ja jetzt einführen...", "author" => "Tobi"],
                ["text" => "Ich wollts dir geben!", "author" => "Monet"],
            ];


        foreach ($quotes as $quote) {
            yield QuoteCreateAction::run(new QuoteCreateTransfer($quote));
        }
    }

    public function asCommand(Command $command): void
    {
        foreach ($this->handle() as $quote) {
            $command->info('Imported: '.$quote->toQuoteString());
        }
    }
}
