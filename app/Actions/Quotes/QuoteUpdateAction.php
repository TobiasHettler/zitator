<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Events\QuoteRemoved;
use App\Events\QuoteUpdated;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteRemoveTransfer;
use App\Transfers\QuoteUpdateTransfer;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteUpdateAction
{
    use AsAction;

    public string $commandSignature = 'quote:edit {uuid} {--text=} {--author=}';

    public function handle(QuoteUpdateTransfer $transfer): Quote
    {
        event(new QuoteUpdated($transfer));

        return Quote::uuid($transfer->uuid);
    }

    public function asCommand(Command $command): void
    {
        $quote = $this->handle(new QuoteUpdateTransfer([
                'uuid' => $command->argument('uuid'),
                'text' => $command->option('text'),
                'author' => $command->option('author'),
            ])
        );

        $command->info('Updated!');
        $command->info($quote->toQuoteString());
    }
}
