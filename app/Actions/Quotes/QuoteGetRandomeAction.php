<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteGetRandomeAction
{
    use AsAction;

    public function handle(?string $author = null): ?Quote
    {
        $query = Quote::query();

        if ($author) {
            $query->where('author', $author);
        }

        return $query->orderByRaw('RAND()')->first();
    }
}
