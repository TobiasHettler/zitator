<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteGetCountByAuthorAction
{
    use AsAction;

    public string $commandSignature = 'quote:rank ';

    public function handle(): Collection
    {
        return Quote::query()
            ->selectRaw('Count(*) as count, author')
            ->groupBy('author')
            ->orderBy('count','desc')
            ->limit(50)
            ->get();
    }

    public function asCommand(Command $command): void
    {
        $result = $this->handle();
        $command->table(['Quote Count', 'Author'], $result->toArray());
    }
}
