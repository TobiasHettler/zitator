<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteCreateAction
{
    use AsAction;

    public string $commandSignature = 'quote:add {author} {text}';

    public function handle(QuoteCreateTransfer $transfer): Quote
    {
        $uuid = (string) Uuid::uuid4();

        event(new QuoteCreated($uuid, $transfer));

        return Quote::uuid($uuid);
    }

    public function asCommand(Command $command): void
    {
        $this->handle(new QuoteCreateTransfer([
                'author' => $command->argument('author'),
                'text' => $command->argument('text'),
            ])
        );

        $command->info('Done!');
    }
}
