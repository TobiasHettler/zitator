<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteListTransfer;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteListAction
{
    use AsAction;

    public string $commandSignature = 'quote:list {--author=} {--quote=} {--orderBy=} {--orderDirection=asc} {--limit=25} {--strict}';

    public function handle(QuoteListTransfer $transfer): Collection
    {
        $query = Quote::query()
            ->limit($transfer->limit);

        if (!$transfer->orderBy) {
            $query = $query->orderByRaw('RAND()');
        }

        if ($transfer->orderBy) {
            $query = $query->orderBy($transfer->orderBy, $transfer->orderDirection);
        }

        if ($transfer->author) {
            $query = $this->flexWhere($query, 'author', $transfer->author, $transfer->strict);
        }

        if ($transfer->text) {
            $query = $this->flexWhere($query, 'text', $transfer->text, $transfer->strict);
        }

        return $query->get();
    }

    public function asCommand(Command $command): void
    {
        $quotes = $this->handle(new QuoteListTransfer([
            'author' => $command->option('author'),
            'text' => $command->option('quote'),
            'orderBy' => $command->option('orderBy'),
            'orderDirection' => $command->option('orderDirection'),
            'limit' => (int) $command->option('limit'),
            'strict' => $command->option('strict'),
        ]));

        $tableData = $quotes->map(fn(Quote $quote) => [
            'uuid' => $quote->uuid, 'author' => $quote->author, 'text' => $quote->text,
            'created_at' => $quote->created_at
        ]);

        $command->table(['uuid', 'author', 'text', 'created_at'], $tableData->toArray());
    }

    protected function flexWhere(Builder $query, string $column, string $search, bool $strict = false): Builder
    {
        if ($strict) {
            return $query->where($column, $search);
        }

        return $query->where($column, 'like', "%$search%");
    }
}
