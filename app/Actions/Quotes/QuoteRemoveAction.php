<?php


namespace App\Actions\Quotes;


use App\Events\QuoteCreated;
use App\Events\QuoteRemoved;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteRemoveTransfer;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;
use Ramsey\Uuid\Uuid;

class QuoteRemoveAction
{
    use AsAction;

    public string $commandSignature = 'quote:remove {uuid}';

    public function handle(QuoteRemoveTransfer $transfer): bool
    {
        if (!Quote::query()->where('uuid', $transfer->uuid)->exists()) {
            return false;
        }

        event(new QuoteRemoved($transfer));

        return true;
    }

    public function asCommand(Command $command): void
    {
        $this->handle(new QuoteRemoveTransfer([
                'uuid' => $command->argument('uuid'),
            ])
        );

        $command->info('Removed!');
    }
}
