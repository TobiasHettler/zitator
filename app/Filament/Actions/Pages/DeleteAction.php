<?php

namespace App\Filament\Actions\Pages;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Callable_;

class DeleteAction extends \Filament\Pages\Actions\DeleteAction
{
    protected $deletionCallback;

    protected function setUp(): void
    {
        parent::setUp();

        $this->action(function (): void {
            $result = $this->process($this->deletionCallback);

            if (!$result) {
                $this->failure();

                return;
            }
            $this->success();
        });
    }

    public function handleDelete(callable $callable)
    {
        $this->deletionCallback = $callable;
        return $this;
    }

}
