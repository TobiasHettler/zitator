<?php

namespace App\Filament\Resources\QuoteResource\Pages;

use App\Actions\Quotes\QuoteRemoveAction;
use App\Actions\Quotes\QuoteUpdateAction;
use App\Filament\Actions\Pages\DeleteAction;
use App\Filament\Resources\QuoteResource;
use App\Models\Quote;
use App\Transfers\QuoteRemoveTransfer;
use App\Transfers\QuoteUpdateTransfer;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Database\Eloquent\Model;

class EditQuote extends EditRecord
{
    protected static string $resource = QuoteResource::class;

    protected function getActions(): array
    {
        return [
            DeleteAction::make()
                ->handleDelete(static fn(Quote $record) => QuoteRemoveAction::run(
                    new QuoteRemoveTransfer([
                        'uuid' => $record->uuid,
                    ])
                )),
        ];
    }

    protected function handleRecordUpdate(Model $record, array $data): Model
    {
        $params = [
            'uuid' => $record->uuid,
            'author' => $data['author'],
            'text' => $data['text'],
        ];

        return QuoteUpdateAction::run(new QuoteUpdateTransfer($params));
    }
}
