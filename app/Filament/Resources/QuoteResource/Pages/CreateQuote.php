<?php

namespace App\Filament\Resources\QuoteResource\Pages;

use App\Actions\Quotes\QuoteCreateAction;
use App\Filament\Resources\QuoteResource;
use App\Transfers\QuoteCreateTransfer;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;

class CreateQuote extends CreateRecord
{
    protected static string $resource = QuoteResource::class;

    protected function handleRecordCreation(array $data): Model
    {
        return QuoteCreateAction::run(new QuoteCreateTransfer($data));
    }
}
