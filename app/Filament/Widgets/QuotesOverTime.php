<?php

namespace App\Filament\Widgets;

use App\Models\Quote;
use App\Support\Color;
use Filament\Widgets\LineChartWidget;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class QuotesOverTime extends LineChartWidget
{
    protected static ?string $heading = 'Quotes over Time';

    protected static ?string $pollingInterval = null;

    public ?string $filter = 'this_year';
    protected int | string | array $columnSpan = '3';

    protected function getData(): array
    {
        $data = null;
        $colors = Color::randomColors();
        [$start, $end] = match ($this->filter) {
            'this_month' => [now()->startOfMonth(), now()->endOfMonth()],
            'last_month' => [now()->previous('month')->startOfMonth(), now()->previous('month')->endOfMonth()],
            'this_year' => [now()->startOfYear(), now()->endOfYear()],
            'last_year' => [now()->previous('year')->startOfYear(), now()->previous('year')->endOfYear()],
            'all' => [Quote::query()->select('created_at')->orderBy('created_at')->first()->created_at, now()],
        };
        $dataSet = Quote::query()
            ->groupBy('author')
            ->select('author')
            ->get()
            ->map(function (Quote $quote, int $key) use ($start, $end, $colors, &$data) {
                $data = Trend::query(
                    Quote::query()->where('author', $quote->author)
                )
                    ->between(
                        start: $start,
                        end: $end,
                    )
                    ->interval(
                        match ($this->filter) {
                            'this_month', 'last_month' => 'day',
                            default => 'month'
                        }
                    )
                    ->count();


                return [
                    'label' => $quote->author,
                    'data' => $data->map(fn(TrendValue $value) => $value->aggregate),
                    'borderColor' => $colors[$key],
                    'backgroundColor' => $colors[$key],
                ];
            });

        return [
            'datasets' => $dataSet->toArray(),
            'labels' => $data->map(fn(TrendValue $value) => $value->date),
        ];
    }

    protected function getFilters(): ?array
    {
        return [
            'this_month' => 'This Month',
            'last_month' => 'Last Month',
            'this_year' => 'This Year',
            'last_year' => 'Last Year',
            'all' => 'All Time',
        ];
    }
}
