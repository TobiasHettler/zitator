<?php

namespace App\Filament\Widgets;

use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Models\Quote;
use App\Support\Color;
use Filament\Widgets\DoughnutChartWidget;

class QuoteDistribution extends DoughnutChartWidget
{
    protected static ?string $heading = 'Total Author Quotes';

    protected static ?string $pollingInterval = null;
    protected int | string | array $columnSpan = 1;
    protected function getData(): array
    {
        $result = QuoteGetCountByAuthorAction::run();
        $data = $result->map(fn (Quote $quote) => $quote->count);
        $labels = $result->map(fn (Quote $quote) => $quote->author);

        return [
            'datasets' => [
                [
                    'label' => 'Quotes per Author',
                    'data' => $data,
                    'backgroundColor' => Color::randomColors(),
                ],

            ],
            'labels' => $labels,
        ];
    }
}
