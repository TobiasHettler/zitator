<?php

namespace App\Filament\Widgets;

use App\Models\Quote;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class QuotesOverview extends BaseWidget
{
    protected static ?string $pollingInterval = null;

    protected int | string | array $columnSpan = 1;

    protected function getCards(): array
    {
        return [
           $this->totalQuotesCard(),
            Card::make('Unique Authors',  Quote::query()->select('author')->groupBy('author')->get()->count())
            ->icon('heroicon-s-user'),
        ];
    }

    public function totalQuotesCard(): Card
    {
        $lastWeek = Quote::query()->whereBetween('created_at', [
            now()->subDays(14),
            now()->subDays(7),
        ])->count();

        $thisWeek = Quote::query()->whereBetween('created_at', [
            now()->subDays(7),
            now(),
        ])->count();

        return Card::make('Total Quotes', Quote::count())
            ->description('In the last 7 days ' . $thisWeek .' new quotes.')
            ->descriptionIcon($lastWeek > $thisWeek ? 'heroicon-s-trending-down' : 'heroicon-s-trending-up')
            ->color($lastWeek > $thisWeek ? 'danger' : 'success');
    }
}
