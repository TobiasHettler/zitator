<?php

namespace App\Filament\Pages;

use App\Filament\Widgets\QuoteDistribution;
use App\Filament\Widgets\QuotesOverTime;
use App\Filament\Widgets\QuotesOverview;
use Filament\Facades\Filament;
use Filament\Pages\Page;
use Filament\Pages\Dashboard as BasePage;
use Filament\Widgets\AccountWidget;

class Dashboard extends BasePage
{
    protected function getWidgets(): array
    {
        return [
            AccountWidget::class,
            QuotesOverview::class,
            QuoteDistribution::class,
            QuotesOverTime::class,
        ];
    }
}
