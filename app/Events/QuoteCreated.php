<?php

namespace App\Events;

use App\Transfers\QuoteCreateTransfer;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class QuoteCreated extends ShouldBeStored
{

    public function __construct(public string $uuid, public QuoteCreateTransfer $data)
    {
        //
    }
}
