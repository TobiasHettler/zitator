<?php

namespace App\Events;

use App\Discord\DTOs\DiscordInteractionTransfer;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class QuizCreated extends ShouldBeStored
{

    public function __construct(
        public string $uuid,
        public string $quoteUuid,
        public array $options,
    )
    {
        //
    }



}
