<?php

namespace App\Events;

use App\Discord\DTOs\DiscordInteractionTransfer;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class CommandExecuted extends ShouldBeStored
{

    public function __construct(
        public string $soure,
        public DiscordInteractionTransfer $data)
    {
        //
    }



}
