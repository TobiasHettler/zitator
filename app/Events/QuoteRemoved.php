<?php

namespace App\Events;

use App\Transfers\QuoteCreateTransfer;
use App\Transfers\QuoteRemoveTransfer;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class QuoteRemoved extends ShouldBeStored
{
    public function __construct(public QuoteRemoveTransfer $data)
    {
        //
    }



}
