<?php

namespace App\Events;

use App\Discord\DTOs\DiscordInteractionTransfer;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class QuizResolved extends ShouldBeStored
{

    public function __construct(
        public string $uuid,
        public string $answer,
        public bool $correct,
        public string $userId
    )
    {
        //
    }



}
