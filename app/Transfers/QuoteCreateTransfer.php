<?php


namespace App\Transfers;


use Spatie\DataTransferObject\DataTransferObject;

class QuoteCreateTransfer extends DataTransferObject
{
    public string $text;
    public string $author;
}
