<?php


namespace App\Transfers;


use Spatie\DataTransferObject\DataTransferObject;

class QuoteListTransfer extends DataTransferObject
{
    public ?string $author;
    public ?string $text;
    public ?string $orderBy;
    public string $orderDirection = 'asc';
    public int $limit = 25;
    public bool $strict = false;
}
