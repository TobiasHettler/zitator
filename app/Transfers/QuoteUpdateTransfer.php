<?php


namespace App\Transfers;


use Spatie\DataTransferObject\DataTransferObject;

class QuoteUpdateTransfer extends DataTransferObject
{
    public string $uuid;
    public ?string $text;
    public ?string $author;
}
