<?php


namespace App\Transfers;


use Spatie\DataTransferObject\DataTransferObject;

class QuoteRemoveTransfer extends DataTransferObject
{
    public string $uuid;
}
