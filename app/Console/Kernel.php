<?php

namespace App\Console;

use App\Actions\Quotes\QuoteCreateAction;
use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Actions\Quotes\QuoteImportAction;
use App\Actions\Quotes\QuoteListAction;
use App\Actions\Quotes\QuoteRemoveAction;
use App\Actions\Quotes\QuoteUpdateAction;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        QuoteCreateAction::class,
        QuoteImportAction::class,
        QuoteRemoveAction::class,
        QuoteUpdateAction::class,
        QuoteGetCountByAuthorAction::class,
        QuoteListAction::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
