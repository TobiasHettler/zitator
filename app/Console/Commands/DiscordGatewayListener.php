<?php

namespace App\Console\Commands;

use App\Discord\CommandResponse;
use App\Discord\DiscordGatewayClient;
use Discord\Discord;
use Discord\Parts\Channel\Message;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use React\EventLoop\Factory;
use React\Socket\Server;

class DiscordGatewayListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discord:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new DiscordGatewayClient())
            ->runConnection();
    }
}
