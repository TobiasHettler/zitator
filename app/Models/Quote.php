<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function uuid(string $uuid): ?self
    {
        return static::where('uuid', $uuid)->first();
    }

    public function toQuoteString(): string
    {
        $year = $this->created_at->year;
        return "\"$this->text\" - $this->author $year";
    }
}
