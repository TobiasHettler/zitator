<?php

namespace App\Projectors;

use App\Events\QuoteCreated;
use App\Events\QuoteRemoved;
use App\Events\QuoteUpdated;
use App\Models\Quote;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class QuoteProjector extends Projector
{
    public function onQuoteCreated(QuoteCreated $event)
    {
        Quote::create([
            'uuid' => $event->uuid,
            'text' => $event->data->text,
            'author' => $event->data->author,
        ]);
    }

    public function onQuoteRemoved(QuoteRemoved $event)
    {
        Quote::uuid($event->data->uuid)->delete();
    }

    public function onQuoteUpdate(QuoteUpdated $event)
    {
        $updateData = array_filter($event->data->except('uuid')->toArray(), fn($data) => $data !== null);

        Quote::uuid($event->data->uuid)
            ->update($updateData);
    }
}
