<?php

namespace App\Support;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

enum Color: string
{
    case RedLight = '#fca5a5';
    case RedNormal = '#dc2626';
    case RedDark = '#7f1d1d';

    case OrangeLight = '#fdba74';
    case OrangeNormal = '#ea580c';
    case OrangeDark = '#7c2d12';

    case AmberLight = '#fcd34d';
    case AmberNormal = '#d97706';
    case AmberDark = '#78350f';

    case YellowLight = '#fde047';
    case YellowNormal = '#ca8a04';
    case YellowDark = '#713f12';

    case LimeLight = '#bef264';
    case LimeNormal = '#65a30d';
    case LimeDark = '#365314';

    case GreenLight = '#86efac';
    case GreenNormal = '#16a34a';
    case GreenDark = '#14532d';

    case EmeraldLight = '#6ee7b7';
    case EmeraldNormal = '#059669';
    case EmeraldDark = '#064e3b';

    case TealLight = '#5eead4';
    case TealNormal = '#0d9488';
    case TealDark = '#134e4a';

    case CyanLight = '#67e8f9';
    case CyanNormal = '#0891b2';
    case CyanDark = '#164e63';

    case SkyLight = '#7dd3fc';
    case SkyNormal = '#0284c7';
    case SkyDark = '#0c4a6e';

    case BlueLight = '#93c5fd';
    case BlueNormal = '#2563eb';
    case BlueDark = '#1e3a8a';

    case IndigoLight = '#a5b4fc';
    case IndigoNormal = '#4f46e5';
    case IndigoDark = '#312e81';

    case VioletLight = '#c4b5fd';
    case VioletNormal = '#7c3aed';
    case VioletDark = '#4c1d95';

    case PurpleLight = '#d8b4fe';
    case PurpleNormal = '#9333ea';
    case PurpleDark = '#581c87';

    case FuchsiaLight = '#f0abfc';
    case FuchsiaNormal = '#c026d3';
    case FuchsiaDark = '#701a75';

    case PinkLight = '#f9a8d4';
    case PinkNormal = '#db2777';
    case PinkDark = '#831843';

    case RoseLight = '#fda4af';
    case RoseNormal = '#e11d48';
    case RoseDark = '#881337';

    public static function randomColors(?int $count = null): array
    {
        return collect(static::cases())
            ->shuffle()
            ->when($count, fn(Collection $collection) => $collection->take($count))
            ->map(fn(Color $color) => $color->value)
            ->toArray();
    }
}
