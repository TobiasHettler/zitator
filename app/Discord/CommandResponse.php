<?php


namespace App\Discord;


use Illuminate\Support\Carbon;

class CommandResponse
{
    public const COLORS = [16098851, 16312092, 9131818, 8311585, 4289797, 12390624, 4886754, 5301186, 12118406];

    public string|null $content = null;
    public array|null $embeds = null;
    public bool $singleEmbed = false;

    public static function create(): self
    {
        return new static();
    }

    public function simpleContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function addEmbeds(CommandResponseEmbed $emebed): self
    {
        $this->embeds[] = $emebed;
        return $this;
    }


    public function response(): array
    {
        $response = [
            "type" => 4,
            "data" => []
        ];

        if ($this->content) {
            $response['data']['content'] = $this->content;
        }

        if ($this->singleEmbed) {
            $response['data']['embed'] = $this->embeds[0]->toArray();
            return $response;
        }

        if ($this->embeds) {
            $response['data']['embeds'] =
                array_map(fn(CommandResponseEmbed $embed) => $embed->toArray(), $this->embeds);
        }

        return $response;
    }

    /**
     * @param  bool  $singleEmbed
     * @return CommandResponse
     */
    public function singleEmbed(bool $singleEmbed = true): CommandResponse
    {
        $this->singleEmbed = $singleEmbed;
        return $this;
    }

}
