<?php


namespace App\Discord;


use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandHook;
use App\Discord\Commands\DiscordCommand;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Discord\Exceptions\CommandException;
use App\Events\CommandExecuted;
use Discord\Discord;
use Discord\InteractionResponseType;
use Discord\InteractionType;
use Discord\Parts\Channel\Message;
use Discord\Parts\WebSockets\MessageReaction;
use Discord\WebSockets\Event;
use Discord\WebSockets\Intents;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use JetBrains\PhpStorm\Pure;
use function Composer\Autoload\includeFile;

class DiscordGatewayClient
{
    public const INTERACTION_CREATE = 'INTERACTION_CREATE';
    public array $commands;
    public Discord $discord;
    public DiscordHttpClient $http;

    public function __construct()
    {
        $this->http = new DiscordHttpClient();
        $this->commands = $this->cacheCommands(config('discord.commands'));
        $this->discord = new Discord([
            'token' => config('discord.bot_token'),
            ]);
        $this->discord->on('ready', fn(Discord $discord) => $this->onConnectionReady($discord));
    }

    protected function onConnectionReady(Discord $discord): void
    {
        echo "Bot is ready!", PHP_EOL;
        $this->registerEvents($discord);
    }

    protected function registerEvents(Discord $discord)
    {
        $discord->on('raw', function (\stdClass $message, Discord $discord) {
            if ($message->t !== static::INTERACTION_CREATE) {
                return;
            }

            $interaction = DiscordGatewayClient::transformMessagetContentToDto($message);
            event(new CommandExecuted('discord', $interaction));

            $cachedCommand = $this->getCommandFromSignature($interaction->data->name);
            $command = $this->instintiateCommand($cachedCommand['class'], $interaction);

            $response = $this->handle($command);

            $this->respond($interaction, $response);

            if ($cachedCommand['watch']) {
                $this->fetchMessage($cachedCommand['class'], $interaction, $command->meta);
            }
        });

        $discord->on(Event::MESSAGE_CREATE, function (Message $message, Discord $discord) {

            if ($message->author->id !== config('discord.app_id')) {
                return;
            }

            if (!$this->isMessageCached($message->id)) {
                return;
            }

            $cachedMessage = ($this->getMessage($message->id));
            $command = $cachedMessage['command'];
            $watch = 'watch';

            if ($createdHook = $command::$watch['onCreated'] ?? false) {
                $command::$createdHook(
                    $message,
                    $this->http,
                    fn() => $this->removeMessageFromCache($message->id),
                    $cachedMessage['meta']
                );
                return;
            }
        });

        $discord->on(Event::MESSAGE_REACTION_ADD, function (MessageReaction $reaction, Discord $discord) {
            if ($reaction->user_id === config('discord.app_id')) {
                return;
            }

            if (!$this->isMessageCached($reaction->message_id)) {
                return;
            }

            $cachedMessage = ($this->getMessage($reaction->message_id));
            $command = $cachedMessage['command'];
            $watch = 'watch';

            if ($reactionAddHook = $command::$watch[Event::MESSAGE_REACTION_ADD] ?? false) {

                $command::$reactionAddHook(
                    $reaction,
                    $this->http,
                    fn() => $this->removeMessageFromCache($reaction->message_id),
                    $cachedMessage['meta']);
                return;
            }
        });
    }

    public function runConnection()
    {
        $this->discord->run();
    }

    public function handle(DiscordCommand $command): CommandResponse
    {
        return $command->handle();
    }

    protected function callAfterHook(
        string $hook,
        DiscordCommand $command,
        array $message
    ): void {
        $command->$hook($message, $this->http);
    }

    public function respond(DiscordInteractionTransfer $interaction, CommandResponse $response): void
    {
        $this->http->intractionRespond($interaction, $response);
    }

    public static function transformMessagetContentToDto(\stdClass $message): DiscordInteractionTransfer
    {
        $interaction = get_object_vars($message->d);
        $interaction['data'] = new DiscordInteractionCommandTransfer(get_object_vars($interaction['data']));
        return new DiscordInteractionTransfer($interaction);
    }

    protected function cacheCommands(array $commands): array
    {
        return collect($commands)
            ->mapWithKeys(function (string $commandClass) {
                $ref = (new \ReflectionClass($commandClass));

                return [
                    $this->extractCommandSignature($ref) => [
                        'class' => $commandClass,
                        'watch' => !empty($commandClass::$watch),
                    ]
                ];
            })
            ->toArray();
    }

    protected function extractCommandSignature(\ReflectionClass $reflectionClass): string
    {
        $attributes = $reflectionClass
            ->getAttributes(Command::class);

        throw_if(empty($attributes), new CommandException($reflectionClass->getName().' is not a Command'));

        return $attributes[0]->newInstance()->name;
    }

    protected function getCommandFromSignature(string $signature): array
    {
        return $this->commands[$signature];
    }

    protected function instintiateCommand(string $command, DiscordInteractionTransfer $interaction): DiscordCommand
    {
        return new ($command)($interaction, $this);
    }

    protected function fetchMessage(string $command, DiscordInteractionTransfer $interactionTransfer, $meta = []): array
    {
//       $this->discord->getHttpClient()->patch('/webhooks/'.config('discord.app_id').'/'.$interactionTransfer->token.'/messages/@original', [])
//        ->then(function ($message) use ($command, $meta) {
//            ray($message);
//            return $this->saveMessage($message, $command, $meta);
//        });
        $message = $this->http->updateInteractionMessage($interactionTransfer);
        return $this->saveMessage($message, $command, $meta);
    }

    protected function saveMessage(array $message, string $command, array $meta = []): array
    {
        Cache::add(
            'watchedMessage.'.$message['id'],
            [
                'command' => $command,
                'message' => $message,
                'meta' => $meta
            ],
            now()->addDays(15)
        );

        return $message;
    }

    protected function isMessageCached(string $messageId): bool
    {
        return Cache::has('watchedMessage.'.$messageId);
    }

    protected function getMessage(string $messageId): array
    {
        return Cache::get('watchedMessage.'.$messageId);
    }

    protected function removeMessageFromCache(string $messageId): void
    {
        Cache::forget('watchedMessage.'.$messageId);
    }

    #[Pure]
    public function createResponse(): CommandResponse
    {
        return CommandResponse::create();
    }

}
