<?php


namespace App\Discord;


use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Discord\DTOs\DiscordMessageTransfer;
use Discord\Parts\Channel\Message;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DiscordHttpClient
{

    protected string $baseApi = '';
    protected string $botToken = '';
    protected string $appId = '';

    protected array $routes = [
        'respond' => 'interactions/$interaction_id/$interaction_token/callback',
        'addReaction' => 'channels/$channel_id/messages/$message_id/reactions/$reaction/@me',
        'updateInteractionMessage' => 'webhooks/$app_id/$interaction_token/messages/@original',
        'updateMessage' => 'channels/$channel_id/messages/$message_id',
        'deleteAllReactions' => 'channels/$channel_id/messages/$message_id/reactions',
        'getUser' => 'users/$user_id',
    ];

    public function __construct()
    {
        $this->baseApi = config('discord.base_api');
        $this->appId = config('discord.app_id');
        $this->botToken = config('discord.bot_token');
    }

    public function intractionRespond(DiscordInteractionTransfer $interaction, CommandResponse $response): void
    {
        $this->http()->post(
            $this->getRoute('respond',
                ['interaction_id' => $interaction->id, 'interaction_token' => $interaction->token]),
            $response->response()
        );
    }

    public function updateInteractionMessage(DiscordInteractionTransfer $interaction, array $data = []): array
    {
        return $this->http()
            ->patch($this->getRoute('updateInteractionMessage',
                ['app_id' => $this->appId, 'interaction_token' => $interaction->token]), $data)
            ->json();

    }

    public function updateMessage(DiscordMessageTransfer $message, CommandResponse $data): array
    {
        return $this->http()
            ->patch($this->getRoute('updateMessage',
                ['channel_id' => $message->channel_id, 'message_id' => $message->id]), $data->response()['data'])
            ->json();

    }

    public function addReaction(Message $message, string $reaction): void
    {
        $this->http()
            ->put($this->getRoute('addReaction', [
                'message_id' => $message['id'], 'channel_id' => $message['channel_id'],
                'reaction' => urlencode($reaction)
            ]));
    }

    public function deleteAllReactions(DiscordMessageTransfer $message): void
    {
        $re= $this->http()
            ->delete($this->getRoute('deleteAllReactions', [
                'message_id' => $message->id, 'channel_id' => $message->channel_id,
            ]));
    }

    /**
     * @param  string  $userId
     * @return \Illuminate\Http\Client\Response
     */
    public function getUser(string $userId):\Illuminate\Http\Client\Response
    {
      return $this->http()
            ->get($this->getRoute('getUser', [
                'user_id' => $userId,
            ]));
    }

    protected function http(): PendingRequest
    {
        return Http::withToken(config('discord.bot_token'), 'Bot');
    }

    protected function getRoute(string $name, array $params = []): string
    {
        $route = trim($this->routes[$name]);

        foreach ($params as $key => $param) {
            $route = str_replace('$'.$key, $param, $route);
        }

        return $this->baseApi.'/'.$route;
    }
}
