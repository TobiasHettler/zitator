<?php


namespace App\Discord\Exceptions;

use Illuminate\Validation\ValidationException;
use Throwable;

class CommandValidationException extends ValidationException
{

}
