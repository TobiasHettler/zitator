<?php


namespace App\Discord;


use App\Models\Quote;
use MathieuViossat\Util\ArrayToTextTable;

/** @method CommandResponse respond() */
trait UseQuoteEmbed
{
    public function getColors(): array
    {
        return [16098851, 16312092, 9131818, 8311585, 4289797, 12390624, 4886754, 5301186, 12118406];
    }

    public function getRandomColor(): string
    {
        return $this->getColors()[array_rand($this->getColors(), 1)];
    }

    public function quoteResponse(Quote $quote): CommandResponse
    {
        return $this->respond()->addEmbeds(
            CommandResponseEmbed::create()
                ->title('"'.$quote->text.'"')
                ->color($this->getRandomColor())
                ->footer($quote->author.' - '.$quote->created_at->format('F Y'), asset("/img/quote.png"))
        );
    }

    public function creatTable(array $array): string
    {
        $renderer = new ArrayToTextTable($array);
        return '```'.$renderer->getTable().'```';
    }
}
