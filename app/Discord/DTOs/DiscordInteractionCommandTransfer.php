<?php


namespace App\Discord\DTOs;


use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\FlexibleDataTransferObject;

class DiscordInteractionCommandTransfer extends FlexibleDataTransferObject
{
    public string $id;
    public string $name;
    public ?array $options;
}
