<?php


namespace App\Discord\DTOs;

use Spatie\DataTransferObject\FlexibleDataTransferObject;

class DiscordInteractionTransfer extends FlexibleDataTransferObject
{
    public string $id;
    public int $type;
    public ?DiscordInteractionCommandTransfer $data;
    public ?string $guild_id;
    public ?string $channel_id;
    public ?string $application_id;
    public ?\stdClass $member;
    public ?\stdClass $user;
    public string $token;
    public int $version;
}
