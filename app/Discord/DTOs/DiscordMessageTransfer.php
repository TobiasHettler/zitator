<?php


namespace App\Discord\DTOs;


use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\FlexibleDataTransferObject;

class DiscordMessageTransfer extends FlexibleDataTransferObject
{
    public string $id;
    public string $channel_id;
}
