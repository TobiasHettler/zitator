<?php

namespace App\Discord\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class Command
{
    public function __construct(
        public string $name,
    ) {
    }
}
