<?php

namespace App\Discord\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class CommandHook
{
    public const HOOK_BEFORE = 'before';
    public const HOOK_AFTER = 'after';

    public function __construct(public string $hook)
    {
    }
}
