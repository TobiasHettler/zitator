<?php

namespace App\Discord\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class CommandOption
{
    public function __construct(
        public string $help,
        public ?string $name = null,
        public bool $required = false,
        public array|string|null $validate = null,
        public array $validateMessages = []
    ) {
    }
}
