<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Actions\Quotes\QuoteListAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use App\Transfers\QuoteListTransfer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

#[Command(name: 'list')]
class DiscordCommandList extends DiscordCommand
{
    use UseQuoteEmbed;

    protected int $color;

    #[CommandOption('Filter the list and only get quotes from the specified Author.')]
    protected ?string $author;

    #[CommandOption('Filter the list and only get quotes from that match the quote.')]
    protected ?string $quote;

    #[CommandOption('If enabeld the filter options must match exactly the author or quote.')]
    protected bool $strict = false;

    #[CommandOption('Specify if the list should be ordered ascending or desecending.')]
    protected string $orderdirection = 'asc';

    #[CommandOption('Define the column that should be used for sorting')]
    protected ?string $orderby;

    #[CommandOption('Include the quote ids in the List')]
    protected bool $withid = false;

    public function __construct(
        protected DiscordInteractionTransfer $interaction,
        private DiscordGatewayClient $client)
    {
        parent::__construct($this->interaction, $this->client);
        $this->color = $this->getRandomColor();
    }

    public function handle(): CommandResponse
    {
        $params = [
            'limit' => 15,
            'author' => $this->author,
            'text' => $this->quote,
            'strict' => $this->strict,
            'orderDirection' => $this->orderdirection,
            'orderBy' => $this->orderby,
        ];

        /** @var Collection $quotes */
        $quotes = QuoteListAction::run(new QuoteListTransfer($params));

        return $this->respond($this->createListTable($quotes, $this->withid));
    }

    /**
     * @param  Collection  $result
     * @param  bool|null  $withId
     * @return string
     */
    protected function createListTable(Collection $result, ?bool $withId = null): string
    {
        if ($withId) {
            $tableData = $result->map(fn(Quote $quote) => [
                'id' => $quote->id,
                'author' => $quote->author,
                'text' => Str::limit($quote->text, '50'),
            ]);
            return $this->creatTable($tableData->toArray());
        }

        $tableData = $result->map(fn(Quote $quote) => [
            'author' => $quote->author,
            'text' => Str::limit($quote->text, '50'),
        ]);

        return $this->creatTable($tableData->toArray());
    }
}
