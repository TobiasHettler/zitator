<?php


namespace App\Discord\Commands;

use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Discord\Exceptions\CommandException;
use App\Discord\Exceptions\CommandValidationException;
use Discord\Interaction;
use Illuminate\Support\Facades\Validator;

abstract class DiscordCommand
{
    public static array $watch = [];
    public array $meta = [];
    protected DiscordInteractionCommandTransfer $commandRequest;

    public function __construct(
        protected DiscordInteractionTransfer $interaction,
        private DiscordGatewayClient $client
    ) {
        $this->commandRequest = $this->interaction->data;

        $this->resolveOptions($this->commandRequest);
    }

    abstract public function handle(): CommandResponse;

    protected function respond(?string $message = null): CommandResponse
    {
        $response = $this->client->createResponse();

        if ($message) {
            return $response->simpleContent($message);
        }

        return $response;
    }

    private function resolveOptions(DiscordInteractionCommandTransfer $commandRequest): void
    {
        $reflectionClass = new \ReflectionObject($this);


        foreach ($reflectionClass->getProperties() as $property) {
            $attributes = $property->getAttributes(CommandOption::class);

            if (empty($attributes)) {
                continue;
            }

            $attribute = $attributes[0];

            /** @var CommandOption $option */
            $option = $attribute->newInstance();

            $optionName = $option->name ?? $property->getName();

            $optionValue = $this->extractOptionFromCommandRequest($optionName);

            $this->validateOption($option, $optionName, $optionValue);

            if ($option->required && $optionValue === null) {
                throw  new CommandException("Option '$optionName' is required!");
            }

            $this->assignValueToProperty($property, $optionValue);
        }
    }

    protected function assignValueToProperty(\ReflectionProperty $property, mixed $value): void
    {
        if ($value !== null) {
            $this->{$property->getName()} = $value;
            return;
        }

        if ($property->getType()->allowsNull()) {
            $this->{$property->getName()} = $value;
            return;
        }

        if ($property->hasDefaultValue()) {
            return;
        }

        throw new CommandException('Property '.$property->getName().' could not be hydrated.');

    }

    protected function validateOption(CommandOption $option, string $optionName, mixed $optionValue): bool
    {
        if (!$option->validate) {
            return true;
        }

        $validator = Validator::make(
            [$optionName => $optionValue],
            [$optionName => $option->validate],
            $option->validateMessages
        );

        if ($validator->fails()) {
            throw new CommandValidationException($validator);
        }
    }

    protected function extractOptionFromCommandRequest(string $key): mixed
    {
        if (!$this->commandRequest->options) {
            return null;
        }

        $option = collect($this->commandRequest->options)
            ->first(fn(\stdClass $option) => $option->name === $key);

        return $option === null ? null : trim($option->value);
    }

    public function meta(string $key, $value): self
    {
        $this->meta[$key] = $value;
        return $this;
    }

}
