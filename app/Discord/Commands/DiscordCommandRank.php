<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Discord\Attributes\Command;
use App\Discord\CommandResponse;
use App\Discord\CommandResponseEmbed;
use App\Discord\DiscordGatewayClient;
use App\Discord\DTOs\DiscordInteractionTransfer;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use Illuminate\Database\Eloquent\Collection;

#[Command(name: 'rank')]
class DiscordCommandRank extends DiscordCommand
{
    use UseQuoteEmbed;

    protected int $color;

    public function __construct(
        protected DiscordInteractionTransfer $interaction,
        private DiscordGatewayClient $client
    ) {
        parent::__construct($interaction, $client);
        $this->color = $this->getRandomColor();
    }

    public function handle(): CommandResponse
    {
        /** @var Collection $result */
        $result = QuoteGetCountByAuthorAction::run();

        if ($result->count() === 0) {
            return $this->respond('There are no Quotes, yet.');
        }

        return $this->respond()
            ->addEmbeds($this->createPodium($result))
            ->addEmbeds(CommandResponseEmbed::create()
                ->description($this->createRankTable($result))
                ->color($this->color)
            );
    }

    /**
     * @param  Collection  $result
     * @return array
     */
    protected function createRankTable(Collection $result): string
    {
        $rankTableData = $result->map(fn(Quote $quote, int $key) => [
            'Rank' => ++$key, 'Author' => $quote->author, 'Quotes' => $quote->count
        ])->toArray();

        return $this->creatTable($rankTableData);
    }

    /**
     * @param  Collection  $result
     */
    protected function createPodium(Collection $result): CommandResponseEmbed
    {
        $embed = CommandResponseEmbed::create()
            ->title(':trophy: '.$result->first()->author.' is the Zitator :trophy:')
            ->description('__________________')
            ->color($this->color)
            ->field(
                ":first_place: ".$result->first()->author,
                $result->first()->count." Quotes",
                true
            );

        if ($secondPlace = $result->get(1, null)) {
            $embed->field(
                ":second_place: ".$secondPlace->author,
                $secondPlace->count." Quotes",
                true
            );
        }

        if ($thirdPlace = $result->get(2, null)) {
            $embed->field(
                ":third_place: ".$thirdPlace->author,
                $thirdPlace->count." Quotes",
                true
            );
        }

        return $embed;
    }

}
