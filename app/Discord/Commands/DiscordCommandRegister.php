<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteCreateAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\CommandResponseEmbed;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use App\Models\User;
use App\Transfers\QuoteCreateTransfer;
use Filament\Facades\Filament;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

#[Command(name: 'register')]
class DiscordCommandRegister extends DiscordCommand
{
    #[CommandOption(
        help: 'Your Email.',
        required: true
    )]
    protected string $email;

    #[CommandOption(
        help: 'Your Password',
        required: true
    )]
    protected string $password;

    public function handle(): CommandResponse
    {
        if (!$this->interaction->user) {
            return $this->respond()->addEmbeds(
                CommandResponseEmbed::create()
                    ->color(15548997)
                    ->title('This command needs to be run in Zitators private messages.')
            );
        }

        $discordUser = $this->interaction->user;
        $validator = Validator::make(['email' => $this->email, 'discord_id' => $discordUser->id], [
            'email' => [Rule::unique('users', 'email'), 'email'],
            'discord_id' => [Rule::unique('users', 'discord_id')]
        ]);

        if (!$validator->passes()) {
            $response = $this->respond();

            foreach ($validator->messages()->toArray() as $key => $message) {
                $response->addEmbeds(
                    CommandResponseEmbed::create()
                        ->color(15548997)
                        ->title(Str::ucfirst($key).' validation Error!')
                        ->description(implode(' ', $message))
                );
            }

            return $response;
        }

        $user = User::create([
            'name' => $discordUser->username,
            'email' => $this->email,
            'discord_id' => $discordUser->id,
            'password' => Hash::make($this->password),
        ]);


        return $this->respond()->addEmbeds(
            CommandResponseEmbed::create()
                ->title('Success! Click here and visit the dashboard.')
                ->url(URL::temporarySignedRoute(
                    'checkin', now()->addMinutes(30), ['user' => $user->id]
                ))
                ->color(5763719)
        );
    }
}
