<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteGetCountByAuthorAction;
use App\Actions\Quotes\QuoteListAction;
use App\Actions\Quotes\QuoteUpdateAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\DTOs\DiscordInteractionCommandTransfer;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use App\Transfers\QuoteListTransfer;
use App\Transfers\QuoteUpdateTransfer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

#[Command(name: 'update')]
class DiscordCommandUpdate extends DiscordCommand
{
    use UseQuoteEmbed;

    #[CommandOption(
        required: true,
        help: 'The uuid of the quote that should be updated.'
    )]
    protected string $uuid;

    #[CommandOption(
        help: 'The updated author.'
    )]
    protected ?string $author;

    #[CommandOption(
        help: 'The updated quote.'
    )]
    protected ?string $quote;

    public function handle(): CommandResponse
    {
        $params = [
            'uuid' => $this->uuid,
            'author' => $this->author,
            'text' => $this->quote,
        ];

        $quote = QuoteUpdateAction::run(new QuoteUpdateTransfer($params));

        return $this->quoteResponse($quote)->simpleContent('uuid: '.$quote->uuid);
    }

}
