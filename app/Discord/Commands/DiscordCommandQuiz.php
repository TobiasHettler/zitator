<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandHook;
use App\Discord\CommandResponse;
use App\Discord\CommandResponseEmbed;
use App\Discord\DiscordGatewayClient;
use App\Discord\DiscordHttpClient;
use App\Discord\DTOs\DiscordMessageTransfer;
use App\Discord\UseQuoteEmbed;
use App\Events\QuizCreated;
use App\Events\QuizResolved;
use App\Models\Quote;
use Discord\Parts\Channel\Message;
use Discord\Parts\Channel\Reaction;
use Discord\Parts\WebSockets\MessageReaction;
use Discord\WebSockets\Event;
use Discord\WebSockets\Intents;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

#[Command(name: 'quiz')]
class DiscordCommandQuiz extends DiscordCommand
{
    use UseQuoteEmbed;

    public static array $watch = [
        Event::MESSAGE_REACTION_ADD => 'onReactionAdd',
        'onCreated' => 'onCreated',
        'intents' => Intents::GUILD_MESSAGES
    ];

    public function handle(): CommandResponse
    {

        /* @var Quote $quote */
        $quote = QuoteGetRandomeAction::run();
        $answerOptions = Quote::query()
            ->select('author')
            ->groupBy('author')
            ->where('author', '!=', $quote->author)
            ->orderByRaw('RAND()')
            ->limit(3)
            ->get()
            ->map(fn(Quote $wrongQuote) => [
                'author' => $wrongQuote->author,
                'correct' => false,
            ])
            ->push([
                'author' => $quote->author,
                'correct' => true,
            ])
            ->shuffle();

        $quizUuid = Uuid::uuid4()->toString();

        $this->meta('answerOptions', $answerOptions->all())
            ->meta('quizUuid', $quizUuid)
            ->meta('quote', $quote->text);

        event(new QuizCreated(uuid: $quizUuid, options: $answerOptions->all(), quoteUuid: $quote->uuid));

        return $this->respond()
            ->addEmbeds(CommandResponseEmbed::create()
                ->author('❓🤔❓ Who said? ❓🤔❓')
                ->title($quote->text)
                ->description('```
1️⃣ : '.$answerOptions[0]['author'].'
2️⃣ : '.$answerOptions[1]['author'].'
3️⃣ : '.$answerOptions[2]['author'].'
4️⃣ : '.$answerOptions[3]['author'].'
```
❓❓❓❓❓❓❓❓❓❓')

            );
    }

    public static function onCreated(
        Message $message,
        DiscordHttpClient $client
    ) {
        $message->react('1️⃣')
            ->done(fn() => $message->react('2️⃣')
                ->done(fn() => $message->react('3️⃣')
                    ->done(fn() => $message->react('4️⃣'))));
    }


    public static function onReactionAdd(
        MessageReaction $reaction,
        DiscordHttpClient $client,
        callable $stopWatching,
        array $meta
    ): void {
        $stopWatching();
        $answerOptions = $meta['answerOptions'];
        $quote = $meta['quote'];
        $quizUuid = $meta['quizUuid'];

        $correctIndex = collect($answerOptions)->search(fn($answer) => $answer['correct']);

        $isCorrect = match ($reaction->emoji->name) {
            '1️⃣' => $correctIndex === 0,
            '2️⃣' => $correctIndex === 1,
            '3️⃣' => $correctIndex === 2,
            '4️⃣' => $correctIndex === 3,
        };

        event(new QuizResolved(uuid: $quizUuid, answer: $reaction->emoji->name, correct: $isCorrect,
            userId: $reaction->user_id));

        $slogen = $isCorrect ? '🏆 '.$client->getUser($reaction->user_id)->json()['username'].' guessed correct. 🏆'
            : '🤦🏾‍♀️ '.$client->getUser($reaction->user_id)->json()['username'].' guessed '.$reaction->emoji->name.' wrong. 🤦🏼‍♂️';

        $footer = $isCorrect ? '🎉🎊🎉🎊🎉🎊🎉🎊🎉🎊'
            : '🤦🏾‍♀️🤦🏿🤦🏼‍♂️🙂🙃🙂🙃🙂🙃🤦🏾‍♀️🤦🏿🤦🏼‍♂️';
        $message = new DiscordMessageTransfer(['id' => $reaction->message_id, 'channel_id' => $reaction->channel_id]);

        $client->updateMessage(
            $message,
            CommandResponse::create()
                ->singleEmbed()
                ->addEmbeds(CommandResponseEmbed::create()
                    ->author('❓🤔❓ Who said? ❓🤔❓')
                    ->title($quote)
                    ->description('```
1️⃣ '.($answerOptions[0]['correct'] ? '✅' : '❌').': '.$answerOptions[0]['author'].'
2️⃣ '.($answerOptions[1]['correct'] ? '✅' : '❌').': '.$answerOptions[1]['author'].'
3️⃣ '.($answerOptions[2]['correct'] ? '✅' : '❌').': '.$answerOptions[2]['author'].'
4️⃣ '.($answerOptions[3]['correct'] ? '✅' : '❌').': '.$answerOptions[3]['author'].'
```
'.$slogen.'

'.$footer)));

        $client->deleteAllReactions($message);
    }

}
