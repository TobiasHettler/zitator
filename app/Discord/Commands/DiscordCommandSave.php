<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteCreateAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use App\Transfers\QuoteCreateTransfer;
use Illuminate\Http\JsonResponse;

#[Command(name: 'save')]
class DiscordCommandSave extends DiscordCommand
{
    use UseQuoteEmbed;

    #[CommandOption(
        help: 'The Quote that you want to save.',
        required: true
    )]
    protected string $quote;

    #[CommandOption(
        help: 'The author of the Quote',
        required: true
    )]
    protected string $author;

    public function handle(): CommandResponse
    {
        /** @var Quote $quote */
        $quote = QuoteCreateAction::run(new QuoteCreateTransfer(['text' => $this->quote, 'author' => $this->author]));
        return $this->quoteResponse($quote)->simpleContent('uuid: '. $quote->uuid);
    }
}
