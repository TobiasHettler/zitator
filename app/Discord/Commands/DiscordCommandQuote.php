<?php


namespace App\Discord\Commands;


use App\Actions\Quotes\QuoteGetRandomeAction;
use App\Discord\Attributes\Command;
use App\Discord\Attributes\CommandOption;
use App\Discord\CommandResponse;
use App\Discord\UseQuoteEmbed;
use App\Models\Quote;
use Illuminate\Http\JsonResponse;

#[Command(name: 'quote')]
class DiscordCommandQuote extends DiscordCommand
{
    use UseQuoteEmbed;

    #[CommandOption(help: 'The author from which the quote should be written.', name: 'user')]
    protected ?string $author = null;

    public function handle(): CommandResponse
    {
        /** @var Quote $quote */
        $quote = QuoteGetRandomeAction::run($this->author);

        if (!$quote) {
            return $this->respond('There are no Quotes from '. $this->author .', yet.');
        }

        return $this->quoteResponse($quote);
    }
}
