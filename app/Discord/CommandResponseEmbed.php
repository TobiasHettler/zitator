<?php


namespace App\Discord;


use Illuminate\Support\Carbon;

class CommandResponseEmbed
{
    public const COLORS = [16098851, 16312092, 9131818, 8311585, 4289797, 12390624, 4886754, 5301186, 12118406];

    public string|null $title = null;
    public string|null $description = null;
    public string|null $url = null;
    public int|null $color = null;
    public Carbon|null $timestamp = null;
    public array|null $footer = null;
    public string|null $thumbnail = null;
    public string|null $image = null;
    public string|null $author = null;
    public string|null $author_url = null;
    public string|null $author_icon = null;
    public array|null $fields = null;

    public static function create(): self
    {
        return new static();
    }


    public function title(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function description(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function url(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function color(int $color): self
    {
        $this->color = $color;
        return $this;
    }

    public function timestamp(Carbon $time): self
    {
        $this->timestamp = $time;
        return $this;
    }

    public function footer(string $text, string $icon): self
    {
        $this->footer = [
            'icon_url' => $icon,
            'text' => $text
        ];

        return $this;
    }

    public function thumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    public function image(string $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function author(string $author): self
    {
        $this->author = $author;
        return $this;
    }

    public function authorIcon(string $icon): self
    {
        $this->author_icon = $icon;
        return $this;
    }

    public function authorUrl(string $url): self
    {
        $this->author_url = $url;
        return $this;
    }

    public function field(string $name, string $value, bool $inline = false): self
    {
        $this->fields[] = [
            'name' => $name,
            'value' => $value,
            'inline' => $inline
        ];
        return $this;
    }

    public function toArray(): array
    {
        $embed = [];

        if ($this->title) {
            $embed['title'] = $this->title;
        }

        if ($this->description) {
            $embed['description'] = $this->description;
        }

        if ($this->url) {
            $embed['url'] = $this->url;
        }

        if ($this->color) {
            $embed['color'] = $this->color;
        }

        if ($this->timestamp) {
            $embed['timestamp'] = $this->timestamp->toIso8601String();
        }

        if ($this->footer) {
            $embed['footer'] = $this->footer;
        }

        if ($this->thumbnail) {
            $embed['thumbnail']['url'] = $this->thumbnail;
        }

        if ($this->image) {
            $embed['image']['url'] = $this->image;
        }

        if ($this->author) {
            $embed['author']['name'] = $this->author;
        }

        if ($this->author_url) {
            $embed['author']['url'] = $this->author_url;
        }

        if ($this->author_icon) {
            $embed['author']['icon_url'] = $this->author_icon;
        }

        if ($this->fields) {
            $embed['fields'] = $this->fields;
        }

        return $embed;
    }

}
