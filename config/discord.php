<?php

use App\Discord\Commands\DiscordCommandList;
use App\Discord\Commands\DiscordCommandQuiz;
use App\Discord\Commands\DiscordCommandQuote;
use App\Discord\Commands\DiscordCommandRank;
use App\Discord\Commands\DiscordCommandRegister;
use App\Discord\Commands\DiscordCommandSave;
use App\Discord\Commands\DiscordCommandUpdate;

return [
    'base_api' => 'https://discord.com/api/v8/',
    'public_key' => env('DISCORD_PUBLIC_KEY'),
    'app_id' => env('DISCORD_APPLICATION_ID'),
    'bot_token' => env('DISCORD_BOT_TOKEN'),
    'signiture_validation' => true,
    'commands' => [
        DiscordCommandQuiz::class,
        DiscordCommandQuote::class,
        DiscordCommandSave::class,
        DiscordCommandRegister::class,
        DiscordCommandRank::class,
        DiscordCommandList::class,
        DiscordCommandUpdate::class,
    ]
];
